---
description: Learn how to get involved with the Meltano community.
---

# Community

We aim to build Meltano with the support of the community and we want everyone to feel included.

## Guidelines

### Code of Conduct

Everyone interacting in dbt Slack, codebase, issue trackers, and mailing lists are expected to follow the [PyPA Code of Conduct][conduct]. If you are unable to abide by the code of conduct set forth here, we encourage you not to participate in the community.

### Contributor Guide

We welcome contributions to the project! Please review our [Contributor Guide](/docs/contributor-guide.html) on how to contribute.

## Demo Days

We have Demo Days every other week on Friday at 9 AM Pacific (16:00 UTC). Join us on Zoom to share and hear updates from community members and the Meltano team about how they've been using Meltano. We also have the #demo-day Slack channel for planning and discussion.

Our next demo days are:

* 2021-05-07
* 2021-05-21
* 2021-06-04
* 2021-06-18
* 2021-07-02

Use [this link][gcal] to view the full events calendar. View past office hours in our [YouTube Playlist][demodayplaylist].

## Office Hours

We have Office Hours weekly on Wednesday at 9 AM Pacific (16:00 UTC). Join us on Zoom and bring any questions you have to the team. We will share the Zoom link in Slack and via social media prior to the event. There is sometimes a short presentation about something top of mind from the core team, but otherwise this is your time to chat with us and get some help. Use [this link][gcal] to view the events calendar. We also have the #office-hours Slack channel for planning and discussion.

View past office hours in our [YouTube Playlist][officehoursplaylist].

## Slack

Join the <SlackChannelLink>Meltano Slack workspace<OutboundLink /></SlackChannelLink>, which is frequented by the core team and over 850 community members. You can ask any questions you may have in here, or just chat with fellow users.

[conduct]: https://www.pypa.io/en/latest/code-of-conduct/
[officehoursplaylist]: https://www.youtube.com/playlist?list=PLO0YrxtDbWAtuuubcEz7mnCHoGfIf8voT
[demodayplaylist]: https://www.youtube.com/playlist?list=PLO0YrxtDbWAuLRElrtwFI5PwlAEMUi0AD
[gcal]: https://calendar.google.com/calendar/embed?src=c_01cj48ha4h199ctjefi85t9dgc%40group.calendar.google.com
